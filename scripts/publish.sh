#!/bin/sh
set -eu #-o pipefail

cd "$(dirname "$(readlink -f "${0}")")/.."
export PATH="$(pwd)/node_modules/.bin:${PATH}"

# Read API secrets from file
exec 3<"web-ext-api-secret.txt"
IFS='' read -r API_KEY    <&3
IFS='' read -r API_SECRET <&3 || test $? -lt 128
exec 3<&-

# Push GIT repository to remote
git push
git push --tags

# Launch the `web-ext` tool with the read data
exec web-ext sign --api-key="${API_KEY}" --api-secret="${API_SECRET}" --channel="listed" "$@"